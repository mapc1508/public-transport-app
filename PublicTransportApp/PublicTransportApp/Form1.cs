﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Device.Location;


namespace PublicTransportApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        publicTransportEntities context = new publicTransportEntities();
        GeoCoordinate myLocation = new GeoCoordinate(43.8573452, 18.4060558);//AUBIH
        Dictionary<station, float> nearbyStations = new Dictionary<station, float>();

        private void Form1_Load(object sender, EventArgs e)
        {
            //GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
            //watcher.TryStart(false, TimeSpan.FromMilliseconds(1000));
            //GeoCoordinate coordinates = watcher.Position.Location;
            //if (coordinates.IsUnknown != true)
            //{
            //    myLocation = new GeoCoordinate(coordinates.Latitude, coordinates.Longitude);
            //}
            //else
            //{
            //    MessageBox.Show("Unknown latitude and longitude.");
            //} 
            foreach (var station in context.stations)
            {
                comboBoxStationDeparture.Items.Add(station.name);
            }
            foreach (var station in context.stations)
            {
                if (station.latitude == null || station.longitude == null) continue;
                int distance = (int)myLocation.GetDistanceTo(new GeoCoordinate((float)station.latitude, (float)station.longitude));
                if (distance < 1000) nearbyStations.Add(station, distance);//all stations within a kilometer
            }
            //int y = 0;
            //int minutes = 0;
            //var stationIDs = context.line_station.Where(x => x.line_id == 3).Select(x => x.station_id).ToArray();
            //for (int i = 0; i < 100; i++)
            //{
            //    context.shedules.Add(new shedule() { line_id = 3, station_id = stationIDs[y], arrival = new TimeSpan(7, minutes, 0) });
            //    minutes += 4;
            //    if (y < stationIDs.Length - 1)
            //        y++;
            //    else y = 0;
            //}
            //context.SaveChanges();
        }

        private void checkBoxShowNearest_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxStationDeparture.Items.Clear();
            comboBoxStationDeparture.Text = "";
            comboBoxDestination.Text = "";
            if (checkBoxShowNearest.Checked)
            {
                foreach (var station in nearbyStations.OrderBy(x => x.Value))
                {
                    comboBoxStationDeparture.Items.Add(station.Key.name + " (" + station.Value + " m)");
                }
            }
            else
            {
                foreach (var station in context.stations)
                {
                    comboBoxStationDeparture.Items.Add(station.name);
                }
            }
        }

        private void comboBoxStationsDeparture_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDestination.Items.Clear();
            comboBoxDestination.Text = "";
            string selectedDeparture = comboBoxStationDeparture.SelectedItem.ToString();
            if(checkBoxShowNearest.Checked) selectedDeparture = selectedDeparture.Substring(0, selectedDeparture.IndexOf('('));

            var selectedStationID = context.stations.Single(x => x.name == selectedDeparture).id;
            var endStations = from station in context.stations
                              where station.id > selectedStationID
                              select station.name;
            comboBoxDestination.Items.AddRange(endStations.ToArray());
        }

        Dictionary<int, List<StationSchedule>> directRoutes = new Dictionary<int, List<StationSchedule>>();
        Dictionary<int, List<StationSchedule>> indirectRoutes = new Dictionary<int, List<StationSchedule>>();

        private void buttonShow_Click(object sender, EventArgs e)
        {
            directRoutes.Clear();
            indirectRoutes.Clear();
            treeView1.Nodes.Clear();
            listBoxResultLines.Items.Clear();

            string selectedDeparture = comboBoxStationDeparture.SelectedItem.ToString();
            if (checkBoxShowNearest.Checked) selectedDeparture = selectedDeparture.Substring(0, selectedDeparture.IndexOf('('));
            int startingStationID = context.stations.Single(x => x.name == selectedDeparture).id;

            string selectedDestination = comboBoxDestination.SelectedItem.ToString();
            int endStationID = context.stations.Single(x => x.name == selectedDestination).id;

            TimeSpan? selectedTime = dateTimePicker1.Value.TimeOfDay;

            foreach (var line in context.line_station.Where(x => x.station_id == startingStationID).Select(x => (int)x.line_id).ToArray())
            {
                var routeList = Functions.FilterStations(context, line, startingStationID, endStationID, selectedTime);
                if (routeList.Last().id == endStationID) directRoutes.Add(line, routeList);
                else indirectRoutes.Add(line, routeList);
            }

            if (directRoutes.Count == 0)
            {
                foreach (var lineID in indirectRoutes.Keys)
                {
                    var secondLineIDs = context.line_station.Where(x => x.station_id == endStationID).Select(x => x.line_id);
                    foreach (var secondLineID in secondLineIDs.ToArray())
                    {
                        var sharedStations = from lineOne in indirectRoutes[lineID]
                                             join lineTwo in context.line_station.Where(x => x.line_id == secondLineID)
                                             on lineOne.id equals lineTwo.station_id
                                             select lineOne.id;

                        int[] sharedStationsArray = sharedStations.ToArray();
                        if (sharedStationsArray.Count() != 0)
                        {
                            int? intersection = sharedStationsArray.Last();
                            var lastTime = indirectRoutes[lineID].Last().Arrival;
                            var newRouteList = Functions.FilterStations(context, (int)secondLineID, (int)intersection, endStationID, lastTime);
                            directRoutes.Add(lineID, indirectRoutes[lineID].Union(newRouteList).ToList());
                            break;
                        }
                    }    
                }
            }

            foreach (var line in directRoutes.Keys)
            {
                try
                {
                    var departure = directRoutes[line].First().Arrival;
                    var arrival = directRoutes[line].Last().Arrival;
                    var duration = arrival - departure;
                    if(duration != null)
                    listBoxResultLines.Items.Add(String.Format("{0}) Dep: {1}  |  Arr: {2}  |  Dur:  {3}",line, departure, arrival, duration));
                }
                catch { }
            }
            if (listBoxResultLines.Items.Count == 0) MessageBox.Show("No results found!");
        }

        private void listBoxResultLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            //richTextBox1.Clear();
            treeView1.Nodes.Clear();
            int index = Convert.ToInt32(listBoxResultLines.SelectedItem.ToString().Substring(0, 1));
            Functions.ShowSchedule(directRoutes[index], treeView1);
        }
    }
}
