﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        public static int[] Range { get; private set; }

        static void Main(string[] args)
        {
            Random random = new Random();
            //int[] x = Enumerable.Range(10, 100).ToArray();
            int randomNumber = random.Next(0, 20);
            Console.WriteLine("Factoriel of number {0} is {1}",randomNumber, Factoriel(randomNumber));
        }

        static int Factoriel(int n)
        {
            if (n == 0) return 1;
            return n * Factoriel(n - 1);
        }
    }
}
