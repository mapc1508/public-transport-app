﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Device.Location;

namespace NearestStations
{
    class StationSchedule : station
    {
        public List<TimeSpan?> arrivalTimes = new List<TimeSpan?>();
    }
    class Program
    {
        static void Main(string[] args)
        {
            var context = new publicTransportEntities();
            var myLocation = new GeoCoordinate(43.8514614, 18.4019142);

            //* GeoCoordinateWatcher watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
            // watcher.Start();
            // GeoCoordinate myLocation = watcher.Position.Location;
            // if (!watcher.Position.Location.IsUnknown)
            // {
            //     double lat = myLocation.Latitude;
            //     double lng = myLocation.Longitude;
            // }
            // */

            Dictionary<station, float> listOfNearbyStations = new Dictionary<station, float>();
            foreach (var station in context.stations)
            {
                if (station.latitude == null || station.longitude == null) continue;
                int distance = (int)myLocation.GetDistanceTo(new GeoCoordinate((float)station.latitude, (float)station.longitude));
                if (distance < 1000) listOfNearbyStations.Add(station, distance);//all stations within a kilometer
            }
            listOfNearbyStations.OrderBy(x => x.Value);

            foreach (var station in listOfNearbyStations)
            {
                Console.WriteLine("Station: {0} | Distance: {1} m", station.Key.name, station.Value);
            }

            int startingStation = 18;
            int endStation = 26;
            TimeSpan? currentTime = DateTime.Now.TimeOfDay;
            Dictionary<int, List<StationSchedule>> directRoutes = new Dictionary<int, List<StationSchedule>>();
            Dictionary<int, List<StationSchedule>> indirectRoutes = new Dictionary<int, List<StationSchedule>>();

            foreach (var line in context.line_station.Where(x => x.station_id == startingStation).Select(x => (int)x.line_id).ToArray())
            {
                var routeList = FilterStations(context, line, startingStation, endStation, currentTime);
                if (routeList.Last().id == endStation) directRoutes.Add(line, routeList);
                else indirectRoutes.Add(line, routeList);
            }
            //Console.WriteLine();

            if(directRoutes.Count == 0)
            {
                foreach (var lineID in indirectRoutes.Keys)
                {
                        var secondLineID = context.line_station.First(x => x.station_id == endStation).line_id;
                    
                        var sharedStations = from lineOne in context.line_station.Where(x => x.line_id == lineID)
                                             join lineTwo in context.line_station.Where(x => x.line_id == secondLineID)
                                             on lineOne.station_id equals lineTwo.station_id
                                             select lineOne.station_id;

                        int?[] sharedStationsArray = sharedStations.ToArray();                        
                        if (sharedStationsArray.Count() != 0)
                        {
                            int? intersection = sharedStationsArray.LastOrDefault();

                        var newRouteList = FilterStations(context, (int)secondLineID, (int)intersection, endStation, currentTime);
                        directRoutes.Add(lineID,indirectRoutes[lineID].Union(newRouteList).ToList());
                        }  
                }   
            }
            foreach (var line in directRoutes.Keys)
            {
                ShowSchedule(directRoutes[line].ToList());
            }
        }

        static List<StationSchedule> FilterStations(publicTransportEntities context, int lineID, int firstStationID, int lastStationID, TimeSpan? currentTime)
        {
            var lineName = context.lines.Single(x => x.ID == lineID).name;
            var route = from x in context.line_station
                           where x.line_id == lineID && x.station_id >= firstStationID && x.station_id <= lastStationID
                           join y in context.stations
                           on x.station_id equals y.id
                           select new StationSchedule()
                           {
                               id = y.id,
                               name = y.name + " (Line: " + lineName + ")"
                           };
            List<StationSchedule> routeList = route.ToList();
            foreach (var station in routeList)
            {
                var arrivalTimes = context.shedules.ToList()
                .Where(x => x.station_id == station.id && x.line_id == lineID && x.arrival > currentTime)
                .Select(x => x.arrival);
                station.arrivalTimes = arrivalTimes.ToList();
            }
            return routeList;
        }
        static void ShowSchedule(List<StationSchedule> stations)
        {
            int stationNumber = 1;
            foreach (var station in stations)
            {
                Console.WriteLine("Station #{0}: {1}\n\n Arrivals: ", stationNumber, station.name);
                foreach (var arrival in station.arrivalTimes)
                {
                    Console.WriteLine(" " + arrival.ToString());
                }
                Console.WriteLine();
                ++stationNumber;
            }
        }
    }
}
