﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PublicTransportApp
{
    class Functions
    {
        public static List<StationSchedule> FilterStations(publicTransportEntities context, int lineID, int firstStationID, int lastStationID, TimeSpan? currentTime)
        {
            var lineName = context.lines.Where(x => x.ID == lineID).Select(x => new { x.name, x.company}).First();
            var route = from x in context.line_station
                        where x.line_id == lineID && x.station_id >= firstStationID && x.station_id <= lastStationID
                        join y in context.stations
                        on x.station_id equals y.id
                        select new StationSchedule()
                        {
                            id = y.id,
                            name = y.name, //+ " (Line: " + lineName + ")"
                        };
            
            List<StationSchedule> routeList = route.ToList();
            foreach (var station in routeList)
            {
                try
                {
                    if (station == routeList.First()) station.LineName = String.Format("{0} ({1})", lineName.name, lineName.company);
                    station.Arrival = context.shedules.ToList()
                    .First(x => x.station_id == station.id && x.line_id == lineID && x.arrival > currentTime).arrival;
                    currentTime = station.Arrival;
                }
                catch
                {
                }
            }

            return routeList;
        }

        public static void ShowSchedule(List<StationSchedule> stations, /*RichTextBox richTextBox*/ TreeView treeView)
        {
            int stationNumber = 1;
            var result = new Dictionary<string, List<TreeNode>>();
            string parent = "";
            foreach (var station in stations)
            {
                var children = new List<TreeNode>();

                //if (!String.IsNullOrWhiteSpace(station.LineName))
                //{   
                //    richTextBox.Text = stationNumber == 1 ? 
                //    richTextBox.Text += String.Format("Line: {0}\n\n", station.LineName): 
                //    richTextBox.Text += String.Format("\nLine: {0}  (Change to this line)\n\n", station.LineName);
                //    stationNumber = 1;
                //}   
                //richTextBox.Text += String.Format("    {0}) {1} - {2} \n", stationNumber, station.Arrival, station.name);               
                //++stationNumber;

                if (!String.IsNullOrWhiteSpace(station.LineName))
                {
                    //parent = stationNumber == 1 ?
                    //parent = String.Format("Line: {0}\n\n", station.LineName) :
                    //parent = String.Format("Line: {0}  (Change to this line)\n\n", station.LineName);
                    if(stationNumber == 1)
                    {
                        parent = String.Format("Line: {0}\n\n", station.LineName);
                        result.Add(parent, new List<TreeNode>());
                    }
                    else
                    {
                        parent = String.Format("Line: {0}  (Change to this line)\n\n", station.LineName);
                        result.Add(parent, new List<TreeNode>());
                    }
                    stationNumber = 1;
                }
                //children.Add(new TreeNode(String.Format("    {0}) {1} - {2} \n", stationNumber, station.Arrival, station.name)));
                result[parent].Add(new TreeNode(String.Format("    {0}) {1} - {2} \n", stationNumber, station.Arrival, station.name)));
                //treeView.Nodes.Add(new TreeNode(parent, children.ToArray()));
                ++stationNumber;
            }
            foreach (var item in result.Keys)
            {
                treeView.Nodes.Add(new TreeNode(item, result[item].ToArray()));
            }
        }
    }
}
